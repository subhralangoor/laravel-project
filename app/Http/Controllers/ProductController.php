<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $products = [];
        $str = file_get_contents('assets/products.json');
        if($str != '')
        {
            $products = json_decode($str);
        }

        return view('product.index', compact('products'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'product_name' => 'required',
            'quantity' => 'required|numeric|min:0',
            'price' => 'required|numeric|min:0'
        ]);

        $tempArray = [];

        $data = $request->input();
        unset($data['_token']);
        $data['submission_time'] = date('Y-m-d h:i:s A');
        $data['total_price'] = $data['quantity']*$data['price'];

        $str = file_get_contents('assets/products.json');
        if($str != '')
        {
            $tempArray = json_decode($str);
        }
        array_push($tempArray, $data);
        $jsonData = json_encode($tempArray);
        file_put_contents('assets/products.json', $jsonData);

        $response = ['status' => 'success', 'message' => 'Product added successfully', 'data' => $data];
        if($request->ajax()) {
            return response()->json($response);
        }

        return redirect()->route('products.index')
                        ->with('status', 'Product added successfully');
    }
}
