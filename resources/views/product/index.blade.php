@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        Products
                    </div>

                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <form id="add-product">
                                    <div class="alert alert-success" style="display: none;"></div>
                                    <div class="form-horizontal">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-sm">
                                                <label for="product_name" class="col-md-4 control-label">Product Name*</label>

                                                <div class="col-md-8">
                                                    <input type="text" id="product_name" name="product_name" class="form-control">

                                                    <span class="help-block alert-danger">
                                                        <strong id="error-product_name"></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-horizontal">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-sm">
                                                <label for="quantity" class="col-md-4 control-label">Quantity in stock*</label>

                                                <div class="col-md-8">
                                                    <input type="number" id="quantity" name="quantity" class="form-control" min="0">

                                                    <span class="help-block alert-danger">
                                                        <strong id="error-quantity"></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>

                                    <div class="form-horizontal">
                                        <div class="col-md-6">
                                            <div class="form-group form-group-sm">
                                                <label for="price" class="col-md-4 control-label">Price per item*</label>

                                                <div class="col-md-8">
                                                    <input type="number" id="price" name="price" class="form-control" step="0.01" min="0">

                                                    <span class="help-block alert-danger">
                                                        <strong id="error-price"></strong>
                                                    </span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="clearfix"></div>
                                     <div class="form-horizontal">
                                        <div class="col-md-6">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <button type="submit" name="" class="btn btn-sm btn-primary">Save</button>
                                            <button id="reset-btn" type="reset" name="" class="btn btn-sm btn-default">Reset</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <br>
                        <br>

                        <div class="table-responsive">
                            <table id="table-products" class="table table-striped table-bordered" cellspacing="0" width="100%">
                                <thead>
                                    <tr>
                                        <th>Product name</th>
                                        <th>Quantity in stock</th>
                                        <th>Price per item</th>
                                        <th>Datetime submitted</th>
                                        <th>Total value number</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php
                                        $total_price = 0;
                                    ?>
                                    @if(!empty($products))
                                    @foreach ($products as $key => $value)
                                        <?php
                                            $total_price = $total_price + $value->total_price;
                                        ?>
                                        <tr>
                                            <td>{{ $value->product_name }}</td>
                                            <td>{{ $value->quantity }}</td>
                                            <td>{{ $value->price }}</td>
                                            <td>{{ $value->submission_time }}</td>
                                            <td>{{ $value->total_price }}</td>
                                        </tr>
                                    @endforeach
                                    @else
                                        <tr id="no-record"><td colspan="5" align="center">No products to show</td></tr>
                                    @endif
                                    <tr>
                                        <td colspan="4" align="right"><strong>Total Value numbers</strong></td>
                                        <td><strong id="total_price">{{ $total_price }}</strong></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent

    <script type="text/javascript">
        var base_url = '{{ url('/') }}';

        $( function() {
            $('#reset-btn').click(function() {
                $('#error-product_name').html('');
                $('#error-quantity').html('');
                $('#error-price').html('');
            });

            $('#add-product').on('submit', function(e) {
                e.preventDefault(e);

                $.ajax({
                    type:"POST",
                    url:base_url+'/products',
                    data:$(this).serialize(),
                    dataType: 'json',
                    success: function(data){
                        //console.log(data);
                        if(data.status == 'success')
                        {
                            var total_price = $('#total_price').text();
                            total_price = parseInt(total_price) + data.data.total_price;
                            $('#total_price').text(total_price);
                            $('.alert-success').text(data.message);
                            $('.alert-success').show();
                            $('#no-record').hide();
                            var str = '<tr>\
                                            <td>'+data.data.product_name+'</td>\
                                            <td>'+data.data.quantity+'</td>\
                                            <td>'+data.data.price+'</td>\
                                            <td>'+data.data.submission_time+'</td>\
                                            <td>'+data.data.total_price+'</td>\
                                        </tr>';
                            //console.log(str);
                            $('#table-products tbody').find('tr:last').prev().after(str);
                            $('#error-product_name').html('');
                            $('#error-quantity').html('');
                            $('#error-price').html('');
                            $('#add-product')[0].reset();
                        }
                    },
                    error: function(data){
                        //console.log(data);
                        if(data.status == 422)
                        {
                            $.each(data.responseJSON, function(key, value) {
                                //console.log(value[0]);
                                //console.log(key);
                                $('#error-'+key).html(value[0]);
                            });
                        }
                    }
                })
            });
        } );
    </script>
@endsection