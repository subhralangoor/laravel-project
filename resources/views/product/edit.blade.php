@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! Form::model($hotel, ['url' => 'hotels/'.$hotel->id, 'method' => 'put', 'class' => '']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Update Hotel
                            <div class="pull-right">
                                {{ Form::submit('Update', ['class' => 'btn btn-sm btn-primary']) }}
                                <a href="{{ url('hotels') }}" class="btn btn-sm btn-default" role="button">Cancel</a>
                            </div>
                        </div>

                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            
                            <div class="form-horizontal">
                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('destination_id') ? ' has-error' : '' }}">
                                        {{ Form::label('destination_id', 'Destination *', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::select('destination_id', array('' => 'Select') + $destinations, null, ['class' => 'form-control', 'required', 'autofocus']) }}

                                            @if ($errors->has('destination_id'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('destination_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('hotel_name') ? ' has-error' : '' }}">
                                        {{ Form::label('hotel_name', 'Hotel Name *', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('hotel_name', null, ['class' => 'form-control', 'required']) }}

                                            @if ($errors->has('hotel_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('hotel_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('email') ? ' has-error' : '' }}">
                                        {{ Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::email('email', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        {{ Form::label('phone', 'Phone', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('phone', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('website') ? ' has-error' : '' }}">
                                        {{ Form::label('website', 'Website', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('website', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('website'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('website') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('address') ? ' has-error' : '' }}">
                                        {{ Form::label('address', 'Address', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('address', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('city') ? ' has-error' : '' }}">
                                        {{ Form::label('city', 'City', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('city', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('city'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('state') ? ' has-error' : '' }}">
                                        {{ Form::label('state', 'State', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('state', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('state'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('state') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('country') ? ' has-error' : '' }}">
                                        {{ Form::label('country', 'Country', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('country', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('country'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('country') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('pincode') ? ' has-error' : '' }}">
                                        {{ Form::label('pincode', 'Pincode', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('pincode', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('pincode'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('pincode') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <hr>
                            <div class="pull-right">
                                {{ Form::submit('Update', ['class' => 'btn btn-sm btn-primary']) }}
                                <a href="{{ url('hotels') }}" class="btn btn-sm btn-default" role="button">Cancel</a>
                            </div>

                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
@endsection