@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                {!! Form::open(['url' => 'hotels', 'method' => 'post', 'class' => '']) !!}
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Create Hotel
                            <div class="pull-right">
                                {{ Form::submit('Save', ['class' => 'btn btn-sm btn-primary']) }}
                                <a href="{{ url('hotels') }}" class="btn btn-sm btn-default" role="button">Cancel</a>
                            </div>
                        </div>

                        <div class="panel-body">
                            @if (session('status'))
                                <div class="alert alert-success">
                                    {{ session('status') }}
                                </div>
                            @endif
                            
                            <div class="form-horizontal">
                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('destination_id') ? ' has-error' : '' }}">
                                        {{ Form::label('destination_id', 'Destination *', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::select('destination_id', array('' => 'Select') + $destinations, null, ['class' => 'form-control', 'required', 'autofocus']) }}

                                            @if ($errors->has('destination_id'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('destination_id') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('hotel_name') ? ' has-error' : '' }}">
                                        {{ Form::label('hotel_name', 'Hotel Name *', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('hotel_name', null, ['class' => 'form-control', 'required']) }}

                                            @if ($errors->has('hotel_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('hotel_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('email') ? ' has-error' : '' }}">
                                        {{ Form::label('email', 'Email', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::email('email', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('email'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('email') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('phone') ? ' has-error' : '' }}">
                                        {{ Form::label('phone', 'Phone', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('phone', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('phone'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('phone') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('website') ? ' has-error' : '' }}">
                                        {{ Form::label('website', 'Website', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('website', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('website'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('website') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('address') ? ' has-error' : '' }}">
                                        {{ Form::label('address', 'Address', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('address', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('address'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('address') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('city') ? ' has-error' : '' }}">
                                        {{ Form::label('city', 'City', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('city', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('city'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('city') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('state') ? ' has-error' : '' }}">
                                        {{ Form::label('state', 'State', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('state', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('state'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('state') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('country') ? ' has-error' : '' }}">
                                        {{ Form::label('country', 'Country', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('country', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('country'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('country') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6">
                                    <div class="form-group form-group-sm{{ $errors->has('pincode') ? ' has-error' : '' }}">
                                        {{ Form::label('pincode', 'Pincode', ['class' => 'col-md-4 control-label']) }}

                                        <div class="col-md-8">
                                            {{ Form::text('pincode', null, ['class' => 'form-control']) }}

                                            @if ($errors->has('pincode'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('pincode') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <hr>
                            <div class="pull-right">
                                {{ Form::submit('Save', ['class' => 'btn btn-sm btn-primary']) }}
                                <a href="{{ url('hotels') }}" class="btn btn-sm btn-default" role="button">Cancel</a>
                            </div>

                        </div>
                    </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@endsection

@section('js')
    @parent
    
    <script src="{{ asset('js/booking.js') }}"></script>

    <script type="text/javascript">
        var map = {};

        var states = new Bloodhound({
            datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
            queryTokenizer: Bloodhound.tokenizers.whitespace,
            // `states` is an array of state names defined in "The Basics"
            //local: $.map(states, function(state, i) { map[state.stateName] = state; return { value: state.stateName }; }),
            remote: {
                <?php /*?>url : '{{ URL::action('Admin_PackageController@autocompleteDestinations') }}/%QUERY',<?php */?>
                url : '{{ URL::to('admin/package/autocompleteDestinations') }}/%QUERY',
                filter: function(list) {
                    return $.map(list, function(state, i) { map[state.city] = state; return { value: state.city, id: state.id }; });
                }
            }
        });
         
        // kicks off the loading/processing of `local` and `prefetch`
        states.initialize();
        
        $('#destinationDetailTable .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength: 1
        },
        {
            name: 'states',
            displayKey: 'value',
            // `ttAdapter` wraps the suggestion engine in an adapter that
            // is compatible with the typeahead jQuery plugin
            source: states.ttAdapter()
        }).blur(function(){
            if(map[$(this).val()] == null) {
                $(this).val('');
                $(this).parent().next('input').val('');
            }
        }).bind('typeahead:selected', function(obj, datum, name) {
            $(this).parent().next('input').val(datum.id);
        });
    </script>
@endsection